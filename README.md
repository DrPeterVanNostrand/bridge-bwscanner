# bridge-bwscanner

A tool for measuring the bandwidth of Tor bridges.

# About

The following
(taken from [BridgeDB's README](https://metrics.torproject.org/collector.html))
provides a brief overview of what Tor bridges are:

    "Tor Bridges are special Tor relays which are not listed in the public
    relay directory. They are used to help circumvent censorship by
    providing users with connections to the public relays in the Tor
    network."

This crate measures the bandwidth of bridges by creating a two-hop
circuit through each bridge provided, selects an exit relay with a greater
reported bandwidth than that of the bridge, then cURLs files of various
sizes through the two-hop circuit. Measuring the time taken for each
file download allows us to calculate the average bandwidth for each bridge.

The "report" files output by this crate are saved to `<cwd>/reports/`. Each
report follows the format specified in the
[`bridgebw-spec`](https://gitlab.com/DrPeterVanNostrand/bridge-bwscanner/blob/master/bridgebw-spec.txt).

A large portion of this project is based on the work already done in
measuring Tor circuits' bandwidth, notably the python package
[`bwscanner`](https://github.com/TheTorProject/bwscanner).

# Dependencies

To use this crate you must have `tor` installed and be using Rust nigthly.
You must also have access to bridge descriptor files and have a list of
Tor exit descriptors as well.

# Installation

To build the `bridgebw` CLI tool, run the following:

    $ git clone https://gitlab.com/DrPeterVanNostrand/bridge-bwscanner.git
    $ cd bridge-bwscanner
    $ cargo build --release

# Usage

You can get the CLI usage by running:

    $ ./target/release/bridgebw --help

    bridgebw 1.0
    Measures the bandwidth of Tor bridges

    USAGE:
        bridgebw [FLAGS] [OPTIONS]

    FLAGS:
            --default-bridges    use the same paths to the bridge descriptors as BridgeDB
            --default-exits      use exits from your cached-microdesc-consensus file
            --default-port       set the control port to 9051
        -f, --fast               only use exits with the "Fast" flag
        -h, --help               Prints help information
        -t, --launch-tor         launch a child Tor process for the duration of the scan
            --lookup-port        get the control port from the torrc file
        -V, --version            Prints version information

    OPTIONS:
        -b, --bridge-paths <comma separated paths>    paths to bridge descriptors
        -p, --control-port <control port>             the port where the Tor Control server is listening
            --dl <comma separated list>
                only download files of these sizes during scan; available sizes: 2, 4, 8, 16, 32, 64
            --exit-bw-diff <value>                    exits' bandwidth must be `value` or more kB/s faster than the bridge
        -e, --exit-paths <comma separated paths>      paths to exit descriptors
        -n <value>                                    scan the first `n` bridges
            --pass <value>                            your Tor control password, required if the `HashedControlPassword` option is set in your torrc

# An Explained Example

    $ ./target/release/bridgebw \
        -n 1 \
        --launch-tor \
        --control-port=9051 \
        --pass=my-tor-control-password \
        --fast \
        --bridge-paths=/path/to/bridge/descriptors \
        --default-exits \
        --exit-bw-diff=20 \
        --dl=2,4

- `-n 1` runs the scan for a single bridge
- `--launch-tor` launches a child Tor process for the duration of the scan
- `--control-port=9051` sets the Tor control port to 9051
- `--pass=...` sets the Tor control password
- `--fast` tells the scanner to only use exit relays with the "Fast" status-flag
- `--bridge-paths=...` specifies the paths to the bridge network-status descriptors
- `--default-exits` tells the scanner to get the exits' network-status
descriptors from `<Tor data-directory>/cached-microdesc-consensus`
- `--exit-bw-diff=20` builds circuits where each exit's bandwidth is at
least 20 kB/s faster than that of the bridge
- `--dl=2,4` only downloads bandwidth measurement files of sizes 2MB and 4MB

### Stderr Logs

If the above command runs successfully, the log output should look somthing
like the following:

    Feb 20 11:22:13.431 INFO starting bridge bandwidth scan
    Feb 20 11:22:13.459 INFO starting scan for bridge, bw: 40, fp: BRIDGE_FINGERPRINT
    Feb 20 11:22:13.459 DEBG acquired exit, bw: 60, fp: EXIT_FINGERPRINT
    Feb 20 11:22:14.890 DEBG created two-hop circuit
    Feb 20 11:22:14.890 DEBG fetching file, size: 2
    Feb 20 11:22:36.086 DEBG fetch succeeded, bw: 96.61997660002491, dt: 21.196444794
    Feb 20 11:22:36.086 DEBG fetching file, size: 4
    Feb 20 11:23:28.513 DEBG fetch succeeded, bw: 78.1275151928363, dt: 52.427112137
    Feb 20 11:36:43.172 DEBG closed circuit
    Feb 15 11:36:42.220 INFO created "reports" directory, path: ABS_PATH_TO_CARGO_TOML/reports
    Feb 20 11:36:43.231 INFO created report file, path: reports/2018-02-20-0

### The Report File

Running the above command will save a report file to `<cwd>/reports/`. If
the scan runs successfully, the report file will look something like the
following:

    @type bridge-bandwidth-report
    published 2018-02-19 19:14:34
    bridge BRIDGE_FINGERPINT 40
    exit EXIT_FINGERPRINT 60
    dl-times 2M=22 4M=53 8M=None 16M=None 32M=None 64M=None
    dl-bandwidths 2M=96 4M=78 8M=None 16M=None 32M=None 64M=None
    avg-bandwidth 87

A more detailed explanation of the report file format can be found in the
[`bridgebw-spec`](https://gitlab.com/DrPeterVanNostrand/bridge-bwscanner/blob/master/bridgebw-spec.txt).

# Contributing

Details on how to contribute can be found in `CONTRIBUTING.md`.
