extern crate chrono;
extern crate clap;
extern crate curl;
extern crate dotenv;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate slog;
extern crate slog_term;
extern crate stem;
extern crate tor_file_parser;

mod builder;
mod cli;
mod error;
mod logger;
mod report;
mod scanner;

use scanner::BridgeBwScanner;

fn main() {
    BridgeBwScanner::cli().unwrap().run();
}
