use std::fs::File;
use std::time::{Duration, Instant};

use curl::easy::Easy;
use stem::{ServerSpec, TorCtrl, TorProcess};
use tor_file_parser::DescIter;

use cli::build_scanner_from_cli;
use error::ScanError;
use logger::*;
use report::{Report, ReportEntry};

pub const FILE_SIZES: [u8; 6] = [2, 4, 8, 16, 32, 64];
const BASE_URL: &'static str = "http://bwauth.torproject.org/bwauth.torproject.org";

lazy_static! {
    static ref FILE_URLS: Vec<String> = FILE_SIZES.iter()
        .map(|file_size| format!("{}/{}M", BASE_URL, file_size))
        .collect();
}

pub fn duration_to_seconds(dt: Duration) -> f64 {
    let n_secs = dt.as_secs() as f64;
    let frac = dt.subsec_nanos() as f64 / 10u32.pow(9) as f64;
    n_secs + frac
}

#[derive(Debug)]
pub struct BridgeBwScanner {
    pub tor: Option<TorProcess>,
    pub tc: TorCtrl,
    pub client: Easy,
    pub bridge_paths: Vec<String>,
    pub exits: Vec<(String, u32)>,
    pub n_bridges: Option<u32>,
    pub exit_bandwidth_diff: u32,
    pub download_sizes: Option<Vec<u8>>
}

impl BridgeBwScanner {
    pub fn cli() -> Result<Self, ScanError> {
        build_scanner_from_cli()
    }

    pub fn run(mut self) {
        log_starting_scan();
        let mut report = Report::new();

        for (bridge_fp, bridge_bw) in self.get_bridges() {
            log_starting_scan_for_bridge(&bridge_fp, &bridge_bw);

            let (exit_fp, exit_bw) = match self.get_exit(&bridge_bw) {
                Some(exit) => exit,
                None => continue
            };

            let mut report_entry = ReportEntry::new(
                &bridge_fp, &bridge_bw, &exit_fp, &exit_bw
            );

            let circ_id = match self.create_circuit(&bridge_fp, &exit_fp) {
                Ok(circ_id) => circ_id,
                Err(_) => continue
            };

            for (file_size, url) in FILE_SIZES.iter().zip(FILE_URLS.iter()) {
                if let Some(ref sizes) = self.download_sizes {
                    if !sizes.contains(&file_size) {
                        continue;
                    }
                }

                log_fetching_file(*file_size);

                if let Ok((n_bytes, dt)) = self.fetch(url) {
                    let bw = n_bytes as f64 / 1024.0 / dt;
                    log_fetch_succeeded(dt, bw);
                    report_entry.add_measurement(*file_size, dt, bw);
                } else {
                    log_fetch_failed();
                }
            }

            let _ = self.tc.close_circuit(circ_id).unwrap();
            log_closed_circuit();
            report.add_entry(report_entry);
        }

        report.save();
    }

    fn get_bridges(&self) -> Vec<(String, u32)> {
        let mut bridges = vec![];

        for path in &self.bridge_paths {
            let file = File::open(&path).unwrap();

            let iter = DescIter::from_reader(&file)
                .filter_map(|desc| {
                    let fp = desc.get_fingerprint()?;
                    let bw = desc.w?.bandwidth;
                    Some((fp, bw))
                });

            let bridges_from_desc: Vec<(String, u32)> = match self.n_bridges {
                Some(ref scan_limit) => iter.take(*scan_limit as usize).collect(),
                None => iter.collect()
            };

            bridges.extend(bridges_from_desc);
        }

        bridges.sort_unstable_by_key(|tup| tup.1);
        bridges
    }

    fn get_exit(&mut self, bridge_bw: &u32) -> Option<(String, u32)> {
        let min_exit_bw = bridge_bw + self.exit_bandwidth_diff;

        while !self.exits.is_empty() && self.exits[0].1 < min_exit_bw{
            self.exits.remove(0);
        }

        if self.exits.is_empty() {
            log_bridge_bandwidth_exceeds_exits(bridge_bw);
            None
        } else {
            let exit = self.exits[0].clone();
            log_acquired_exit(&exit.0, &exit.1);
            Some(self.exits[0].clone())
        }
    }

    fn create_circuit(&mut self, bridge_fp: &str, exit_fp: &str) -> Result<u16, ()> {
        let circ_path = vec![
            ServerSpec::from_fp(bridge_fp),
            ServerSpec::from_fp(exit_fp)
        ];

        if let Ok(circ_id) = self.tc.create_circuit(circ_path) {
            log_circuit_created();
            Ok(circ_id)
        } else {
            log_failed_to_create_circuit();
            Err(())
        }
    }

    fn fetch(&mut self, url: &str) -> Result<(usize, f64), ScanError> {
        self.client.url(url).unwrap();
        let mut n_bytes_received = 0;
        let timer = Instant::now();

        {
            let mut transfer = self.client.transfer();

            transfer.write_function(|data| {
                let chunk_length = data.len();
                n_bytes_received += chunk_length;
                Ok(chunk_length)
            }).unwrap();

            let _ = transfer.perform()
                .map_err(|curl_error| ScanError::FetchFailed(curl_error))?;
        }

        let dt = duration_to_seconds(timer.elapsed());
        Ok((n_bytes_received, dt))
    }
}
