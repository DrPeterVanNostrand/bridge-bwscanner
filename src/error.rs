use std::io;

use curl;

#[derive(Debug)]
pub enum ScanError {
    DescriptorFileNotFound(String),
    FetchFailed(curl::Error),
    MissingEnvVar(&'static str),
    NoBridgeDescriptorsSupplied,
    NoControlPortSpecified,
    NoExitDescriptorsSupplied,
    NoExitsFound,
    NoTorProcessRunning,
    TorControllerCouldNotConnect,
    TorControllerCouldNotAuthenticate,
    TorProcessError(io::Error),
    TorrcDoesNotContainControlPort,
    TorrcNotFound,
    TorStartupError(Vec<String>),
}
