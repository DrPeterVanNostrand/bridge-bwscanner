use clap::App;

use builder::ScanBuilder;
use error::ScanError;
use scanner::{BridgeBwScanner, FILE_SIZES};

pub fn build_scanner_from_cli() -> Result<BridgeBwScanner, ScanError> {
    let args = App::new("bridgebw")
        .version("1.0")
        .about("Measures the bandwidth of Tor bridges")
        .args_from_usage(
            "[n_bridges] -n [value] 'scan the first `n` bridges'
            [launch_tor] -t, --launch-tor 'launch a child Tor process for the duration of the scan'
            [control_port] -p, --control-port [control port] 'the port where the Tor Control server is listening'
            [default_port] --default-port 'set the control port to 9051'
            [lookup_port] --lookup-port 'get the control port from the torrc file'
            [default_bridges] --default-bridges 'use the same paths to the bridge descriptors as BridgeDB'
            [bridge_paths] -b, --bridge-paths [comma separated paths] 'paths to bridge descriptors'
            [default_exits] --default-exits 'use exits from your cached-microdesc-consensus file'
            [exit_paths] -e, --exit-paths [comma separated paths] 'paths to exit descriptors'
            [fast] -f, --fast 'only use exits with the \"Fast\" flag'
            [exit_bw_diff] --exit-bw-diff [value] 'exits' bandwidth must be `value` or more kB/s faster than the bridge'
            [dl_sizes] --dl [comma separated list] 'only download files of these sizes during scan, the sizes available: 2, 4, 8, 16, 32, 64'
            [pass] --pass [value] 'your Tor control password, required if the `HashedControlPassword` option is set in your torrc'"
        )
        .get_matches();

    let mut builder = ScanBuilder::new();

    if args.is_present("launch_tor") {
        builder.launch_tor();
    }

    if let Some(port) = args.value_of("control_port") {
        if let Ok(port) = port.parse::<u16>() {
            builder.control_port(port);
        } else {
            panic!("Invalid value for --control-port.");
        }
    }

     if args.is_present("default_port") {
        builder.use_default_control_port();
    }

    if args.is_present("lookup_port") {
        builder.lookup_control_port();
    }

    if let Some(n_bridges) = args.value_of("n_bridges") {
        if let Ok(n) = n_bridges.parse::<u32>() {
            builder.n_bridges(n);
        } else {
            panic!("Invalid value for -n.");
        }
    }

    if args.is_present("default_bridges") {
        builder.use_default_bridge_paths();
    }

    if let Some(paths) = args.value_of("bridge_paths") {
        for path in paths.split(',') {
            builder.add_bridge_path(path);
        }
    }

    if args.is_present("default_exits") {
        builder.use_exits_from_cached_microdesc_consensus();
    }

    if let Some(paths) = args.value_of("exit_paths") {
        for path in paths.split(',') {
            builder.add_exit_path(path);
        }
    }

    if args.is_present("fast") {
        builder.ignore_slow_exits();
    }

    if let Some(diff) = args.value_of("exit_bw_diff") {
        if let Ok(diff) = diff.parse::<u32>() {
            builder.exit_bandwidth_diff(diff);
        } else {
            panic!("Invalid value for --exit-bw-diff.");
        }
    }

    if let Some(dl_sizes) = args.value_of("dl_sizes") {
        let mut sizes = vec![];
        for size in dl_sizes.split(',') {
            if let Ok(size) = size.parse::<u8>() {
                if FILE_SIZES.contains(&size) {
                    sizes.push(size);
                }
            }
        }
        builder.download_sizes(sizes);
    }

    if let Some(pass) = args.value_of("pass") {
        builder.tor_control_password(pass);
    }

    builder.build()
}

