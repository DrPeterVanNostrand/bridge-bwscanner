use std::io::stderr;

use slog::{Drain, Logger};
use slog_term::{FullFormat, PlainSyncDecorator};

lazy_static! {
    pub static ref LOGGER: Logger = {
        let log_decorator = PlainSyncDecorator::new(stderr());
        let drain = FullFormat::new(log_decorator).build().fuse();
        Logger::root(drain, o!())
    };
}

pub fn log_starting_scan() {
    info!(LOGGER, "starting bridge bandwidth scan");
}

pub fn log_starting_scan_for_bridge(bridge_fp: &str, bridge_bw: &u32) {
    info!(
        LOGGER,
        "starting scan for bridge";
        "fp" => &bridge_fp,
        "bw" => &bridge_bw
    );
}

pub fn log_bridge_bandwidth_exceeds_exits(bridge_bw: &u32) {
    warn!(
        LOGGER,
        "bridge bandwidth exceeds all exits, skipping bridge";
        "bw" => bridge_bw
    );
}

pub fn log_acquired_exit(exit_fp: &str, exit_bw: &u32) {
    debug!(
        LOGGER,
        "acquired exit";
        "fp" => &exit_fp,
        "bw" => &exit_bw
    );
}

pub fn log_circuit_created() {
    debug!(LOGGER, "created two-hop circuit");
}

pub fn log_failed_to_create_circuit() {
    error!(LOGGER, "failed to create circuit, skipping bridge");
}

pub fn log_closed_circuit() {
    debug!(LOGGER, "closed circuit");
}

pub fn log_fetching_file(file_size: u8) {
    debug!(LOGGER, "fetching file"; "size" => file_size);
}

pub fn log_fetch_succeeded(dt: f64, bw: f64) {
    debug!(LOGGER, "fetch succeeded"; "dt" => dt, "bw" => bw);
}

pub fn log_fetch_failed() {
    warn!(LOGGER, "fetch failed");
}

pub fn log_empty_report() {
    warn!(LOGGER, "called save on an empty Report, report file will not be written");
}

pub fn log_created_reports_dir(path: &str) {
    info!(LOGGER, "created \"reports\" directory"; "path" => path);
}

pub fn log_created_report_file(file_name: &str) {
    let path = format!("reports/{}", file_name);
    info!(LOGGER, "created report file"; "path" => path);
}

pub fn log_no_measurements_for_bridge(bridge_fp: &str) {
    warn!(
        LOGGER,
        "no bandwidth measurements recorded for bridge, adding empty report entry";
        "fp" => bridge_fp
    );
}
