use std::env;
use std::fs::File;
use std::path::Path;
use std::process::Command;
use std::str;

use curl::easy::Easy;
use dotenv::dotenv;
use stem::{StemError, TorCtrl, TorProcess};
use tor_file_parser::{DescIter, Torrc};

use error::ScanError;
use scanner::BridgeBwScanner;

fn tor_is_running() -> bool {
    let output = Command::new("ps").output().unwrap();
    let lines = str::from_utf8(&output.stdout).unwrap().lines();
    for line in lines {
        if line.split_whitespace().nth(3) == Some("tor") {
            return true;
        }
    }
    false
}

fn default_bridge_descriptor_paths() -> Result<Vec<String>, ScanError> {
    dotenv().ok();
    let home = env!("HOME");

    let mut key = "BRIDGE_AUTHORITY_DIRECTORIES";
    let auth_dirs = env::var(key).map_err(|_|
        ScanError::MissingEnvVar(key)
    )?;

    key = "STATUS_FILE";
    let status_file = env::var(key).map_err(|_|
        ScanError::MissingEnvVar(key)
    )?;

    let paths = auth_dirs.split(',')
        .map(|auth_dir| format!("{}/{}/{}", home, auth_dir, status_file))
        .collect();

    Ok(paths)
}

#[derive(Debug)]
pub struct ScanBuilder {
    launch_tor: bool,
    control_port: u16,
    lookup_control_port: bool,
    n_bridges: Option<u32>,
    use_default_bridge_paths: bool,
    use_exits_from_cached_microdesc_consensus: bool,
    bridge_descriptor_paths: Vec<String>,
    exit_descriptor_paths: Vec<String>,
    ignore_slow_exits: bool,
    exit_bandwidth_diff: u32,
    download_sizes: Option<Vec<u8>>,
    tor_control_password: Option<String>
}

impl Default for ScanBuilder {
    fn default() -> Self {
        ScanBuilder {
            launch_tor: false,
            control_port: 0,
            lookup_control_port: false,
            n_bridges: None,
            use_default_bridge_paths: false,
            use_exits_from_cached_microdesc_consensus: false,
            bridge_descriptor_paths: vec![],
            exit_descriptor_paths: vec![],
            ignore_slow_exits: false,
            exit_bandwidth_diff: 10,
            download_sizes: None,
            tor_control_password: None
        }
    }
}

impl ScanBuilder {
    pub fn new() -> Self { ScanBuilder::default() }

    pub fn launch_tor(&mut self) {
        self.launch_tor = true;
    }

    pub fn control_port(&mut self, control_port: u16) {
        self.control_port = control_port;
    }

    pub fn use_default_control_port(&mut self) {
        self.control_port = 9051;
    }

    pub fn lookup_control_port(&mut self) {
        self.lookup_control_port = true;
    }

    pub fn n_bridges(&mut self, n: u32) {
        self.n_bridges = Some(n);
    }

    pub fn use_default_bridge_paths(&mut self) {
        self.use_default_bridge_paths = true;
    }

    pub fn add_bridge_path<T: Into<String>>(&mut self, path: T) {
        self.bridge_descriptor_paths.push(path.into());
    }

    pub fn use_exits_from_cached_microdesc_consensus(&mut self) {
        self.use_exits_from_cached_microdesc_consensus = true;
    }

    pub fn add_exit_path<T: Into<String>>(&mut self, path: T) {
        self.exit_descriptor_paths.push(path.into());
    }

    pub fn ignore_slow_exits(&mut self) {
        self.ignore_slow_exits = true;
    }

    pub fn exit_bandwidth_diff(&mut self, diff: u32) {
        self.exit_bandwidth_diff = diff;
    }

    pub fn download_sizes(&mut self, sizes: Vec<u8>) {
        self.download_sizes = Some(sizes);
    }

    pub fn tor_control_password(&mut self, pass: &str) {
        self.tor_control_password = Some(pass.to_string());
    }

    // Creates an instance of `BridgeBwScanner` using this builder.
    pub fn build(self) -> Result<BridgeBwScanner, ScanError> {
        let tor = self.build_tor_process()?;
        let control_port = self.build_control_port()?;

        let mut tc = TorCtrl::new(control_port)
            .map_err(|_| ScanError::TorControllerCouldNotConnect)?;

        let auth_res = match self.tor_control_password {
            Some(ref pass) => tc.auth(Some(pass)),
            None => tc.auth(None)
        };

        if auth_res.is_err() {
            return Err(ScanError::TorControllerCouldNotAuthenticate);
        }

        let _ = tc.disable_predicted_circuits().unwrap();
        let _ = tc.close_non_internal_circuits().unwrap();

        let mut client = Easy::new();
        let socks_port = tc.get_socks_port().unwrap();
        let proxy_addr = format!("socks5://127.0.0.1:{}", socks_port);
        client.proxy(&proxy_addr).unwrap();

        let bridge_paths = self.build_bridge_paths()?;
        let exits = self.build_exits(&mut tc)?;
        let n_bridges = self.n_bridges;
        let exit_bandwidth_diff = self.exit_bandwidth_diff;
        let download_sizes = self.download_sizes;

        let scanner = BridgeBwScanner {
            tor, tc, client, bridge_paths,
            exits, n_bridges, exit_bandwidth_diff,
            download_sizes
        };

        Ok(scanner)
    }

    // Launches a child tor process or checks that a tor process is
    // currently running.
    fn build_tor_process(&self) -> Result<Option<TorProcess>, ScanError> {
        if !self.launch_tor {
            if !tor_is_running() {
                return Err(ScanError::NoTorProcessRunning);
            } else {
                return Ok(None);
            }
        }

        match TorProcess::launch() {
            Ok(tor) => Ok(Some(tor)),
            Err(StemError::TorProcessError(e)) =>
                Err(ScanError::TorProcessError(e)),
            Err(StemError::TorStartupError(lines)) =>
                Err(ScanError::TorStartupError(lines)),
            _ => panic!("unreachable")
        }
    }

    // Gets the Tor contorl port, whether it was supplied by the user, is
    // the default port 9051, or should be read from the torrc file.
    fn build_control_port(&self) -> Result<u16, ScanError> {
        let port = if self.control_port != 0 {
            self.control_port.clone()
        } else if self.lookup_control_port {
            let torrc_path = Torrc::find_file().ok_or(
                ScanError::TorrcNotFound
            )?;
            let torrc_file = File::open(torrc_path).unwrap();
            Torrc::new(&torrc_file).control_port.ok_or(
                ScanError::TorrcDoesNotContainControlPort
            )?
        } else {
            return Err(ScanError::NoControlPortSpecified);
        };
        Ok(port)
    }

    // Constructs a list of file paths to the specified bridge descriptors,
    // a ScanError is retured if no bridge descriptors are specified or if
    // any of these descriptor files do not exist.
    fn build_bridge_paths(&self) -> Result<Vec<String>, ScanError> {
        let mut paths = self.bridge_descriptor_paths.clone();

        if self.use_default_bridge_paths {
            let default_paths = default_bridge_descriptor_paths()?;
            paths.extend(default_paths);
        }

        if paths.is_empty() {
            return Err(ScanError::NoBridgeDescriptorsSupplied);
        }

        for path in &paths {
            if !Path::new(&path).is_file() {
                let e = ScanError::DescriptorFileNotFound(path.to_string());
                return Err(e);
            }
        }

        Ok(paths)
    }

    // Maps the exits in the exit descriptor files to a list of
    // (fingerpint, bandwidth) tuples in ascending bandwidth order.
    fn build_exits(&self, tc: &mut TorCtrl) -> Result<Vec<(String, u32)>, ScanError> {
        let mut paths = self.exit_descriptor_paths.clone();

        if self.use_exits_from_cached_microdesc_consensus {
            let consensus_path = format!(
                "{}/cached-microdesc-consensus",
                tc.get_data_directory().unwrap()
            );
            paths.push(consensus_path);
        }

        if paths.is_empty() {
            return Err(ScanError::NoExitDescriptorsSupplied);
        }

        let mut exits = vec![];

        for path in paths {
            let file = File::open(&path)
                .map_err(|_| ScanError::DescriptorFileNotFound(path))?;

            let iter = DescIter::from_reader(file)
                .filter_map(|desc| {
                    if !desc.is_exit() { return None; }
                    if self.ignore_slow_exits && !desc.is_fast() {
                        return None;
                    }
                    let fp = desc.get_fingerprint()?;
                    let bw = desc.w?.bandwidth;
                    Some((fp, bw))
                });

            exits.extend(iter.collect::<Vec<(String, u32)>>());
        }

        if exits.is_empty() { return Err(ScanError::NoExitsFound); }
        exits.sort_unstable_by_key(|tup| tup.1);
        Ok(exits)
    }
}
