use std::env::current_dir;
use std::fs::{create_dir, File, read_dir};
use std::io::Write;
use std::path::{Path, PathBuf};

use chrono::Utc;

use logger::{
    log_created_reports_dir,
    log_created_report_file,
    log_empty_report,
    log_no_measurements_for_bridge
};
use scanner::FILE_SIZES;

/// Contains the bandwidth measurements for a single bridge.
#[derive(Debug)]
pub struct ReportEntry {
    bridge_fp: String,
    bridge_bw: u32,
    exit_fp: String,
    exit_bw: u32,
    fetched_sizes: Vec<u8>,
    times: Vec<f64>,
    bandwidths: Vec<f64>
}

impl ReportEntry {
    pub fn new(bridge_fp: &str, bridge_bw: &u32, exit_fp: &str, exit_bw: &u32) -> Self {
        let bridge_fp = if !bridge_fp.starts_with('$') {
            format!("${}", bridge_fp)
        } else {
            bridge_fp.to_string()
        };

        let exit_fp = if !exit_fp.starts_with('$') {
            format!("${}", exit_fp)
        } else {
            exit_fp.to_string()
        };

        ReportEntry {
            bridge_fp: bridge_fp,
            bridge_bw: bridge_bw.clone(),
            exit_fp: exit_fp,
            exit_bw: exit_bw.clone(),
            fetched_sizes: vec![],
            times: vec![],
            bandwidths: vec![]
        }
    }

    /// Updates this entry with the bandwidth measured from a single file download.
    pub fn add_measurement(&mut self, file_size: u8, dt: f64, bw: f64) {
        self.fetched_sizes.push(file_size);
        self.times.push(dt.ceil());
        self.bandwidths.push(bw.floor());
    }

    // Convert this entry to a String that follow the bridgebw-spec format.
    pub fn to_string(&self) -> String {
        if self.fetched_sizes.is_empty() {
            log_no_measurements_for_bridge(&self.bridge_fp);
        }

        let bridge_line = format!(
            "bridge {} {}",
            self.bridge_fp,
            self.bridge_bw
        );

        let exit_line = format!(
            "exit {} {}",
            self.exit_fp,
            self.exit_bw
        );

        let mut download_times_line = "dl-times ".to_string();
        let mut download_bandwidths_line = "dl-bandwidths ".to_string();

        for file_size in FILE_SIZES.iter() {
            let pos = self.fetched_sizes.iter().position(|fetched_size|
                fetched_size == file_size
            );

            let (dt, bw) = if pos.is_none() {
                let dt = format!("{}M=None ", file_size);
                let bw = format!("{}M=None ", file_size);
                (dt, bw)
            } else {
                let i = pos.unwrap();
                let dt = format!("{}M={} ", file_size, self.times[i]);
                let bw = format!("{}M={} ", file_size, self.bandwidths[i]);
                (dt, bw)
            };

            download_times_line.push_str(&dt);
            download_bandwidths_line.push_str(&bw);
        }

        let avg_bw_line = if !self.fetched_sizes.is_empty() {
            let n_fetches = self.fetched_sizes.len() as f64;
            let bw_total: f64 = self.bandwidths.iter().sum();
            let avg_bw = (bw_total / n_fetches).floor();
            format!("avg-bandwidth {}", avg_bw)
        } else {
            "avg-bandwidth None".to_string()
        };

        format!(
            "{}\n{}\n{}\n{}\n{}",
            bridge_line,
            exit_line,
            download_times_line.trim(),
            download_bandwidths_line.trim(),
            avg_bw_line
        )
    }
}

/// Wraps a vector of `ReportEntry`s and writes them to a file following
/// the format specified in the brdigewb-spec.
#[derive(Debug)]
pub struct Report {
    reports_dir: PathBuf,
    entries: Vec<ReportEntry>
}

impl Report {
    /// Instantiates a new `Report`. No changes are made to the file system
    /// until the `.save()` method is called.
    pub fn new() -> Self {
        let mut reports_dir = current_dir().unwrap();
        reports_dir.push("reports");
        Report { reports_dir, entries: vec![] }
    }

    /// Add a bridge's bandwidth measurements to this Report.
    pub fn add_entry(&mut self, entry: ReportEntry) {
        self.entries.push(entry)
    }

    /// Save this report to the file: `<cwd>/reports/<report name>`.
    pub fn save(&mut self) {
        if self.entries.is_empty() {
            log_empty_report();
            return;
        }

        self.create_reports_dir_if_dne();
        let file_name = self.report_name();
        let path = Path::new(&self.reports_dir).join(&file_name);
        let mut file = File::create(path).unwrap();
        log_created_report_file(&file_name);

        let header = format!(
            "@type bridge-bandwidth-report\n\
            published {}",
            Utc::now().format("%Y-%m-%d %H:%M:%S")
        );

        let entries = self.entries.iter()
            .map(|entry| entry.to_string())
            .collect::<Vec<String>>()
            .join("\n");

        let file_contents = format!("{}\n{}\n", header, entries);
        let _ = file.write_all(file_contents.as_bytes()).unwrap();
    }

    // Creates the the directory: `<cwd>/reports/`, if it does not exist.
    fn create_reports_dir_if_dne(&self) {
        if !self.reports_dir.is_dir() {
            let _ = create_dir(&self.reports_dir).unwrap();
            let path = format!("{}", self.reports_dir.display());
            log_created_reports_dir(&path);
        }
    }

    // Concatenates todays date (YYYY-MM-DD) with this report's index.
    fn report_name(&self) -> String {
        let today = Utc::now().format("%Y-%m-%d").to_string();
        let mut report_index: u8 = 0;

        for entry in read_dir(&self.reports_dir).unwrap() {
            let file_name = entry.unwrap().file_name().into_string().unwrap();
            if file_name.starts_with(&today) {
                let last_index: u8 = file_name.split('-')
                    .nth(3).unwrap()
                    .parse().unwrap();
                report_index = last_index + 1;
            }
        }

        format!("{}-{}", today, report_index)
    }
}
